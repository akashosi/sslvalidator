import ssl 
import socket
from datetime import datetime, timedelta
import os
import psycopg2
import traceback

env_vars = {
	"DB_NAME": 'domainnames',
	"DB_USER": None,
	"DB_PASSWORD": None,
	"DB_HOST": None,
	"DB_TABLE": "sslchecks",
	"HOSTNAMES_LIST": "data/test_domain_names.txt",
	"PORT": 443,
}

context = ssl.create_default_context()

def db_connect():
	# Make db connection
	conn = psycopg2.connect(
		dbname=env_vars["DB_NAME"], 
		user=env_vars["DB_USER"],
		password=env_vars["DB_PASSWORD"], 
		host=env_vars["DB_HOST"]
		)
	cur = conn.cursor()
	return conn, cur

def vars_update():
	# Update env vars
	global env_vars
	for var in env_vars.keys():
		if var in os.environ.keys():
			env_vars[var] = os.environ.get(var)

def get_hostnames(file):
	hostnames = []
	try:
		with open(file) as f:
			while True:
				line = f.readline()   
				if not line:
					break
				if bool(line.strip()): hostnames.append(line.strip())
	except FileNotFoundError as err:
		print(err)
		print("\033[91m" + "Specify a valid path for `HOSTNAMES_LIST` " + "\033[0m")

	return hostnames

def _get_certificate(domain_name, port):
	context = ssl.create_default_context()
	certif = {}
	try:
		with socket.create_connection((domain_name, port), timeout=5) as sock:
			with context.wrap_socket(sock, server_hostname = domain_name) as ssock:
				certif = ssock.getpeercert()
	except Exception as err:
		err = "".join(traceback.format_exception_only(type(err), err)).strip()
		print(f"\033[0m{domain_name}\n\t\033[91m{err}\033[0m")
	return certif


def get_records_for_db(domain_names, port=443):
	current_timestamp = datetime.utcnow()
	data = []
	valid_domains = 0
	total = len(domain_names)
	for domain in domain_names:
		certif = _get_certificate(domain, port)
		if certif:
			certExpires = datetime.strptime(certif['notAfter'], '%b %d %H:%M:%S %Y %Z')
			timeToExpiration = certExpires - datetime.now()
			print(f"{domain}\n\tValid until : {certExpires} ({timeToExpiration}).")
			print(f"\tIssued by: {certif['issuer'][2][0][1]}")
			alternate_names = ",".join([alt_name for _, alt_name in certif['subjectAltName']])
			print(f"\tAlternate names in certificate: {alternate_names}")
			data.append((current_timestamp, domain, timeToExpiration))
			valid_domains += 1
		else:
			data.append((current_timestamp, domain, timedelta(0)))
	
	print(f"Valid certificates: {valid_domains}/{total}")
	return data, valid_domains


def table_exists(cur, table_name):
	cur.execute("select * from information_schema.tables where table_name=%s", (table_name,))
	return bool(cur.rowcount)

def create_table(cur, table_name):
	create_query = f"CREATE TABLE {table_name} (time_index TIMESTAMP, domain_name TEXT, time_to_expiration INTERVAL); CREATE INDEX ON {table_name} (time_index DESC);"
	cur.execute(create_query)
	print("Table %s successfully created" % table_name)

def insert_records(cur, table_name, records):
	if not table_exists(cur, table_name):
		create_table(cur, table_name)
	num_of_records = len(records)
	if num_of_records != 0:
		insert_query = f"INSERT INTO {table_name} VALUES (%s,%s,%s)"
		cur.executemany(insert_query, records)
		print(f"{num_of_records} successfully inserted")

 

def main():
	# Update env vars
	vars_update()

	# connect to db
	conn, cursor = db_connect()
	conn.autocommit = True

	# Check certificates to get records
	hostnames = get_hostnames(env_vars["HOSTNAMES_LIST"])

	records, _ = get_records_for_db(hostnames, env_vars["PORT"])

	# insert records to postgreSQL
	insert_records(cursor, env_vars["DB_TABLE"], records)
	
	# close the connection
	cursor.close()
	conn.close()
	print("Done.")

if __name__ == '__main__':
	main()

	
