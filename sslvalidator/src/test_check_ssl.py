from check_ssl import get_hostnames, get_records_for_db
import pytest


TEST_FILE_CONTENT = """
gitlab.com
google.com

expired.badssl.com
wrong.host.badssl.com
самодеј.мкд
"""

@pytest.fixture
def hostnames():
    hostnames = [
        'gitlab.com',
        'google.com',
        'expired.badssl.com',
        'wrong.host.badssl.com',
        'самодеј.мкд',
    ]
    return hostnames

def test_get_hostnames(hostnames, tmp_path):
    d = tmp_path / "sub"
    d.mkdir()
    p = d / "domain_names.txt"
    p.write_text(TEST_FILE_CONTENT)
    assert get_hostnames(p) == hostnames

def test_get_records_for_db(hostnames):
    _, num_of_valid_certs = get_records_for_db(hostnames)
    assert num_of_valid_certs == 3
    